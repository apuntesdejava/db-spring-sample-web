<%-- 
    Document   : index
    Created on : 15-sep-2017, 16:12:29
    Author     : diego
--%>

<%@page import="com.apuntesdejava.db.jpa.domain.Product"%>
<%@page import="com.apuntesdejava.db.util.ApplicationBeans"%>
<%@page import="com.apuntesdejava.db.service.ProductService"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Products List</h1>
        <% ProductService service = ApplicationBeans.getInstance().getProductService();
            List<Product> products = service.findAll();
        %>
        [<a href="edit.jsp">Nuevo</a>]
        <table>
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                </tr>
            </thead>
            <tbody>
                <% for (Product p : products) {%>
                <tr>
                    <td><%= p.getName()%></td>
                    <td><%= p.getStock()%></td>
                </tr>
                <%}%>
            </tbody>
        </table>
        [<a href="../index.html">Home</a>]

    </body>
</html>
