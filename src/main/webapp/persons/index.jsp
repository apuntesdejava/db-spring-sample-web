<%-- 
    Document   : persons
    Created on : 15-sep-2017, 13:54:45
    Author     : diego
--%>

<%@page import="com.apuntesdejava.db.jdbc.domain.Person"%>
<%@page import="com.apuntesdejava.db.service.PersonService"%>
<%@page import="com.apuntesdejava.db.util.ApplicationBeans"%>
<%@page import="org.apache.commons.lang3.time.DateFormatUtils"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Persons List</h1>
        [<a href="edit.jsp">Nuevo</a>]

        <% ApplicationBeans beans = ApplicationBeans.getInstance();
            PersonService personService = beans.getPersonService();

            List<Person> personList = personService.getList();%>
        <table>
            <thead>
                <tr>
                    <th>
                        Nombre
                    </th>
                    <th>
                        Fecha de nacimiento
                    </th>
                </tr>
            </thead>
            <tbody>
                <%for (Person p : personList) {%>        
                <tr>
                    <td><%=p.getName()%></td>
                    <td><%= DateFormatUtils.format(p.getBirthdate(), "dd/MM/yyyy")%></td>        
                </tr>
                <%}%>
            </tbody>
        </table>
        [<a href="../index.html">Home</a>]


    </body>
</html>
