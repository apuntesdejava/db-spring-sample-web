<%-- 
    Document   : edit
    Created on : 15-sep-2017, 14:08:46
    Author     : diego
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Persons - Edit</title>
    </head>
    <body>
        <h1>Persons - Edit</h1>
        <form method="POST" action="<%= request.getContextPath() %>/persons/servlet">
            <label for="name">Nombre</label>
            <input id="name" name="name" type="text"/>
            <br/>
            <label for="birthdate">Fecha de nacimiento</label>
            <input id="birthdate" name="birthdate" placeholder="dd/mm/aaaa"/>
            <br/>
            <button type="submit">Guardar</button>
        </form>
    </body>
</html>
