/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.product.servlet;

import com.apuntesdejava.db.service.ProductService;
import com.apuntesdejava.db.util.ApplicationBeans;
import com.apuntesdejava.db.util.ParamUtil;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author diego
 */
@WebServlet(name = "ProductServlet", urlPatterns = {"/products/servlet"})
public class ProductServlet extends HttpServlet {

    private static final long serialVersionUID = -1780597346341463633L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductService productService = ApplicationBeans.getInstance().getProductService();
        String name = ParamUtil.getString(request, "name");
        int stock = ParamUtil.getInteger(request, "stock");
        long productId = ParamUtil.getLong(request, "personId");
        boolean isNew = productId == 0;
        if (isNew) {
            productService.create(name, stock);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/products/index.jsp");
        rd.forward(request, response);
    }

}
