/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.service.impl;

import com.apuntesdejava.db.dao.ProductDao;
import com.apuntesdejava.db.service.ProductService;
import com.apuntesdejava.db.jpa.domain.Product;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author diego
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    private final static Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    public ProductServiceImpl() {
        LOGGER.debug("Iniciando servicio");
    }

    @Override
    public List<Product> findAll() {
        return productDao.getList();
    }

    @Override
    public void create(String name, int stock) {
        productDao.create(name, stock);
    }

}
