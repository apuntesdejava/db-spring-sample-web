/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.service.impl;

import com.apuntesdejava.db.dao.PersonDao;
import com.apuntesdejava.db.jdbc.domain.Person;
import com.apuntesdejava.db.service.PersonService;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author diego
 */
@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonDao personDao;

    private final static Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    public PersonServiceImpl() {
        LOGGER.debug("Iniciando servicio");
    }

    @Override
    public List<Person> getList() {
        return personDao.getList();
    }

    @Override
    public void create(String name, Date birthdate) {
        personDao.create(name,birthdate);
    }

}
