/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.dao.impl;

import com.apuntesdejava.db.jpa.domain.Product;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import com.apuntesdejava.db.dao.ProductDao;

/**
 *
 * @author diego
 */
@Repository
public class JpaProductDao implements ProductDao {

    @PersistenceUnit
    private EntityManagerFactory emf;
    private final static Logger LOGGER = LoggerFactory.getLogger(JpaProductDao.class);

    public JpaProductDao() {
        LOGGER.debug("iniciando DAO...");
    }

    public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public List<Product> getList() {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Product> query = em.createNamedQuery("Product.findAll", Product.class);
        return query.getResultList();
    }

    @Override
    public void create(String name, int stock) {
        Product p = new Product();
        p.setName(name);
        p.setStock(stock);

        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(p);
        } finally {
            em.getTransaction().commit();
            em.close();
        }

    }

}
