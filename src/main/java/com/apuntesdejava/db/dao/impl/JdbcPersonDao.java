/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.dao.impl;

import com.apuntesdejava.db.dao.PersonDao;
import com.apuntesdejava.db.jdbc.domain.Person;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author diego
 */
@Repository
public class JdbcPersonDao implements PersonDao {

    private final static Logger LOGGER = LoggerFactory.getLogger(JdbcPersonDao.class);

    private JdbcTemplate jdbcTemplate;

    public JdbcPersonDao() {
        LOGGER.debug("Iniciando DAO...");
    }

    @Autowired
    public void setDataSource(DataSource ds) {
        LOGGER.debug("Cargando DataSource {}", ds);
        jdbcTemplate = new JdbcTemplate(ds);
    }

    @Override
    public List<Person> getList() {
        String sql = "select * from person";
        //ejecuta el query y lo mapea como Objeto Person
        return jdbcTemplate.query(sql, PERSON_ROW_MAPPER);
    }

    @PostConstruct
    /**
     * Después de instanciar esta clase, tratará de crear la tabla PERSON
     */
    public void createTable() {
        LOGGER.debug("-- creando tabla:");
        try {
            jdbcTemplate.execute("create table person(person_id int AUTO_INCREMENT not null  primary key, name varchar(100), birthdate date) ");
        } catch (Exception ex) {
            LOGGER.warn(ex.getMessage());
        }
    }
    /**
     * Este propiedad tendrá el mapeo de ResultSet a Person
     */
    private static final RowMapper<Person> PERSON_ROW_MAPPER = (ResultSet rs, int rowNum) -> {
        Person p = new Person();
        p.setPersonId(rs.getInt("person_id"));
        p.setName(rs.getString("name"));
        p.setBirthdate(new Date(rs.getDate("birthdate").getTime()));
        return p;
    };

    @Override
    public void create(String name, Date birthdate) {
        String sql = "insert into person (name,birthdate) values (?,?) ";
        //inserta un registro, y cada argumento es pasado en los parámetros "?" del SQL
        jdbcTemplate.update(sql, name, birthdate);
    }
}
