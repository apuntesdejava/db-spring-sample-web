/*
 * Copyright 2017 diego.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.db.person.servlet;

import com.apuntesdejava.db.service.PersonService;
import com.apuntesdejava.db.util.ApplicationBeans;
import com.apuntesdejava.db.util.ParamUtil;
import java.io.IOException;
import java.util.Date;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author diego
 */
@WebServlet(name = "PersonServlet", urlPatterns = {"/persons/servlet"})
public class PersonServlet extends HttpServlet {

    private static final long serialVersionUID = 9045877768234316641L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersonService personService = ApplicationBeans.getInstance().getPersonService();
        String name = ParamUtil.getString(request, "name");
        Date birthdate = ParamUtil.getDate(request, "birthdate", "dd/MM/yyyy");
        long personId = ParamUtil.getLong(request, "personId");
        boolean isNew = personId == 0;
        if (isNew) {
            personService.create(name, birthdate);
        }
        RequestDispatcher rd = request.getRequestDispatcher("/persons/index.jsp");
        rd.forward(request, response);
    }

}
